import pytest
from src.data.make_dataset import load_data


@pytest.fixture
def sample_data():
    df = load_data('data/raw/train.parquet')
    return df


def test_dataframe_row_count(sample_data):
    expected_row_count = 60000
    actual_row_count = sample_data.shape[0]
    assert actual_row_count == expected_row_count, f"DataFrame has {actual_row_count} rows, expected {expected_row_count}"