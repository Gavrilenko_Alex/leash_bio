import pickle
import pandas as pd
import mlflow
from sklearn.model_selection import cross_val_score
from xgboost import XGBClassifier
import click
from src.features.build_features import make_descriptors
from src.config import SEED, n_estimators


mlflow.set_tracking_uri("http://127.0.0.1:5001")


def extract_features(df: pd.DataFrame) -> tuple[list[list[int]], list[int]]:
    logged_model = "mlflow-artifacts:/0/7c77dadd0033481b88220e02a1e77d7e/artifacts/one_hot_encoder/onehot_encoder.pkl"
    local_path = mlflow.artifacts.download_artifacts(artifact_uri=logged_model)
    with open(local_path, "rb") as f:
        onehot_encoder = pickle.load(f)

    protein_onehot = onehot_encoder.transform(df["protein_name"].values.reshape(-1, 1))

    features = [
        ecfp + protein
        for ecfp, protein in zip(df["ecfp"].tolist(), protein_onehot.tolist())
    ]
    labels = df["binds"].tolist()
    return features, labels


def train_model(features, labels) -> None:
    xgb_model = XGBClassifier(n_estimators=n_estimators, random_state=SEED)
    cv_scores = cross_val_score(
        xgb_model, features, labels, cv=5, scoring="average_precision"
    )
    map_score = cv_scores.mean()

    mlflow.log_param("n_estimators", n_estimators)
    xgb_model.fit(features, labels)
    mlflow.sklearn.log_model(xgb_model, "xgb_model_with_oh")
    mlflow.log_metric("map_score", map_score)


@click.command()
@click.argument("input_file", type=click.Path(exists=True))
def main(input_file: str) -> None:
    df = make_descriptors(input_file)
    features, labels = extract_features(df)
    train_model(features, labels)


if __name__ == "__main__":
    main()
