import os
import pickle
import pandas as pd
from rdkit import Chem
from xgboost import XGBClassifier
from src.features.build_features import generate_ecfp


def process_and_predict(
    test_file: str = "data/raw/test.csv",
    output_file: str = "data/interim/submission.csv",
    onehot_encoder_path: str = "models/onehot_encoder.pkl",
    model_path: str = "models/xgb_model.bin",
    chunksize: int = 100000,
):

    with open(onehot_encoder_path, "rb") as f:
        onehot_encoder = pickle.load(f)

    xgb_model = XGBClassifier()
    xgb_model.load_model(model_path)

    for df_test in pd.read_csv(test_file, chunksize=chunksize):
        # Generate ECFPs for the molecule_smiles
        df_test["molecule"] = df_test["molecule_smiles"].apply(Chem.MolFromSmiles)
        df_test["ecfp"] = df_test["molecule"].apply(generate_ecfp)

        # One-hot encode the protein_name
        protein_onehot = onehot_encoder.transform(
            df_test["protein_name"].values.reshape(-1, 1)
        )

        # Combine ECFPs and one-hot encoded protein_name
        X_test = [
            ecfp + protein
            for ecfp, protein in zip(df_test["ecfp"].tolist(), protein_onehot.tolist())
        ]

        # Predict the probabilities using the XGBoost model trained with cross-validation
        probabilities = xgb_model.predict_proba(X_test)[:, 1]

        # Create a DataFrame with 'id' and 'probability' columns
        output_df = pd.DataFrame({"id": df_test["id"], "binds": probabilities})

        # Save the output DataFrame to a CSV file
        output_df.to_csv(
            output_file, index=False, mode="a", header=not os.path.exists(output_file)
        )


def main():
    process_and_predict()


if __name__ == "__main__":
    main()
