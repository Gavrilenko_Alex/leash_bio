import pickle
import click
import mlflow
import mlflow.sklearn
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
from sklearn.preprocessing import OneHotEncoder


mlflow.set_tracking_uri("http://127.0.0.1:5001")


def generate_ecfp(molecule, radius=2, bits=1024):
    if molecule is None:
        return None
    return list(AllChem.GetMorganFingerprintAsBitVect(molecule, radius, nBits=bits))


def make_descriptors(input_filepath: str) -> pd.DataFrame:
    df = pd.read_csv(input_filepath)
    df["molecule"] = df["molecule_smiles"].apply(Chem.MolFromSmiles)

    df["ecfp"] = df["molecule"].apply(generate_ecfp)
    onehot_encoder = OneHotEncoder(sparse_output=False)
    onehot_encoder.fit(df["protein_name"].values.reshape(-1, 1))

    with open("models/onehot_encoder.pkl", "wb") as f:
        pickle.dump(onehot_encoder, f)

    with mlflow.start_run() as run:
        mlflow.log_artifact(
            "models/onehot_encoder.pkl", artifact_path="one_hot_encoder"
        )
    return df


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
def main(input_filepath):
    df = make_descriptors(input_filepath)
    df.to_csv("data/processed/train_ecfp.csv", index=False)


if __name__ == "__main__":
    main()
