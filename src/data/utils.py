#  function to save a json file
import json


def save_to_json(dict_object: dict, file_path: str) -> None:
    # Write the metrics data to the JSON file
    with open(file_path, "w") as json_file:
        json.dump(dict_object, json_file, indent=4)
