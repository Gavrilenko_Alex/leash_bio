import click
import logging
from pathlib import Path
import duckdb
import pandas as pd


def load_data(input_filepath: str) -> pd.DataFrame:
    """
    Custom function to process data from input_filepath and save the result to output_filepath.
    """
    con = duckdb.connect()

    df = con.query(
        f"""(SELECT *
                            FROM parquet_scan('{input_filepath}')
                            WHERE binds = 0
                            ORDER BY random()
                            LIMIT 30000)
                            UNION ALL
                            (SELECT *
                            FROM parquet_scan('{input_filepath}')
                            WHERE binds = 1
                            ORDER BY random()
                            LIMIT 30000)"""
    ).df()
    con.close()
    return df


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
def main(input_filepath: str):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")
    df = load_data(input_filepath)
    df.to_csv("data/processed/train.csv", index=False)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    main()
