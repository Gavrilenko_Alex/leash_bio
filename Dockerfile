# Use the official Python image from the Docker Hub
FROM python:3.11.9-slim


WORKDIR /app


COPY . .


RUN pip install flake8 black

# Run lint checks
CMD ["sh", "-c", "flake8 src && black src --check"]
